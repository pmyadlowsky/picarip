require "mkmf"

extension_name = "squisharip"
# dir_config(extension_name)
have_library("sndfile", "sf_open")
have_library("pthread", "pthread_create")
have_library("cdio_cdda", "cdio_cddap_open")
have_library("cdio_paranoia", "cdio_paranoia_init")
create_makefile(extension_name)
