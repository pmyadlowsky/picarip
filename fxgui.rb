#! /usr/bin/ruby

require "rubygems"
require "gtk3"
require "discid"
require "taglib"
require "fileutils"
require "i18n"
require "json"
require "pp"
require "pathname"
require "base64"
require "rmagick"
require "squisharip"

CDROM_INFO = "/proc/sys/dev/cdrom/info"
CDWatchInterval = 1
Debugging = false
DefaultBitrate = 320
DeleteWav = true
ButtonWidth = 100
RipSpan = "1-2"

class TrackFile
	attr_reader :path
	def initialize(path)
		@path = path
		@path =~ /([^\/]+)\/[^\/]+$/
		@volume = $1
		@tags = nil
		end
	def tags
		return @tags if @tags
		@tags = {}
		if @path =~ /\.flac$/
			TagLib::FLAC::File.open(@path) do |file|
				if file.xiph_comment
					@tags = file.xiph_comment.field_list_map
					end
				end
		elsif @path =~ /\.ogg/
			TagLib::Ogg::Vorbis::File.open(@path) do |file|
				if file.tag
					@tags = file.tag.field_list_map
					end
				end
			end
		return @tags
		end
	def tracknum
		self.tags['TRACKNUMBER'][0].to_i
		end
	def basename
		@path =~ /([^\/]+)$/
		return $1
		end
	def duration
		if self.tags['LENGTH']
			sec = (self.tags['LENGTH'][0].to_i / 1000.0).round
		else
			begin
				info = SquishaRip.file_info(@path)
				sec = (info[:frames].to_f / info[:samplerate]).round
			rescue Exception => exc
				return "-:--"
				end
			end
		return ("%d:%02d" % [ sec / 60, sec % 60])
		end
	def title(clean = nil)
		data = self.tags
		return self.basename if data.empty?
		return self.basename unless data['TITLE']
		ttl = data['TITLE'][0]
		return (clean ? asciify(ttl) : ttl)
		end
	def mbid_1(mbid)
		# get first piece of MusicBrainz UUID
		mbid =~ /^([^\-]+)/
		return $1
		end
	def artist(opts = {})
		data = self.tags
		if data['ALBUMARTIST']
			artist = data['ALBUMARTIST'][0]
			mbid = mbid_1(data['MUSICBRAINZ_ALBUMARTISTID'][0])
		elsif data['ARTIST']
			artist = data['ARTIST'][0]
			mbid = mbid_1(data['MUSICBRAINZ_ARTISTID'][0])
		else
			return nil
			end
		artist = asciify(artist) if opts[:clean]
		artist << "::#{mbid}" if opts[:mbid]
		return artist
		end
	def album(opts = {})
		data = self.tags
		album = "Disc ID \"#{@volume}\""
		if data['ALBUM']
			album = data['ALBUM'][0]
			end
		return (opts[:clean] ? asciify(album) : album)
		end
	def label
		if self.tags['LABEL']
			return self.tags['LABEL'][0]
		else
			return nil
			end
		end
	end

class Release
	# MusicBrainz release entity
	def initialize(worker, mbid, disc_obj)
		@worker = worker
		@id = mbid
		@disc = disc_obj
		@tracks = nil
		@cover_art = nil
		@data = musicbrainz_lookup
		end
	def add_tag(pipe, field, value)
		pipe.puts "#{field.upcase}=#{value}" #if value
		end
	def release_artists
		return {} unless @data['artist-credit']
		artists = @data['artist-credit'].map {|a| a['artist']['name']}
		ids = @data['artist-credit'].map {|a| a['artist']['id']}
		return {
			:names => artists.join(","),
			:ids => ids.join(",")
			}
		end
	def track_artists(track)
		return {} unless track['artist-credit']
		names = track['artist-credit'].map {|a| a['artist']['name']}
		ids = track['artist-credit'].map {|a| a['artist']['id']}
		return {
			:names => names.join(","),
			:ids => ids.join(",")
			}
		end
	def add_art(path, type)
		return "" unless path && File::exists?(path)
		return " --import-picture-from=\"#{type}||||#{path}\""
		end
	def encoded_file(tracknum)
		base = "#{@disc.path}/track%02d.cdda" % tracknum
		return "#{base}.ogg" if File::exists?("#{base}.ogg")
		return "#{base}.flac" if File::exists?("#{base}.flac")
		return nil
		end
	def encode_image(path, img_type)
		return nil unless path
		imgs = Magick::Image.read(path)
		return nil if imgs.empty?
		img = imgs[0]
		out = ""
		out << [img_type, img.mime_type.size].pack("LL")
		out << img.mime_type
		out << [0].pack("L") # descrip length
		file = IO::binread(path)
		out << [img.columns, img.rows,
			img.depth, 0, file.size].pack("LLLLL")
		out << file
		return Base64::encode64(out).gsub("\n", "")
		end
	def tag_track(tracknum)
		path = self.encoded_file(tracknum)
		return unless path
		art = self.cover_art(@disc.path, tracknum)
		vorbis = false
		if path =~ /\.flac$/
			cmd = "metaflac --remove-all-tags --import-tags-from=-"
			cmd << self.add_art(art[:front], "3")
			cmd << self.add_art(art[:back], "4")
			cmd << " #{path}"
		else
			cmd = "vorbiscomment -w -R #{path}"
			vorbis = true
			end
		pipe = IO::popen(cmd, "w")
		add_tag(pipe, "album", @data['title'])
		add_tag(pipe, "media", @data['media'][0]['format'])
		add_tag(pipe, "label", self.label)
		add_tag(pipe, "catalognumber", self.catalog_number)
		add_tag(pipe, "totaldiscs", @disc.disc_total)
		add_tag(pipe, "disctotal", @disc.disc_total)
		add_tag(pipe, "discnumber", @disc.disc_num)
		artists = self.release_artists
		add_tag(pipe, "musicbrainz_albumartistid", artists[:ids])
		add_tag(pipe, "albumartist", artists[:names])
		add_tag(pipe, "date", @data['date'])
		add_tag(pipe, "asin", @data['asin'])
		add_tag(pipe, "script", self.script)
		add_tag(pipe, "releasecountry", @data['country'])
		add_tag(pipe, "barcode", @data['barcode'])
		add_tag(pipe, "musicbrainz_albumid", @data['id'])
		add_tag(pipe, "totaltracks", @data['media'][0]['track-count'])
		add_tag(pipe, "tracktotal", @data['media'][0]['track-count'])
		add_tag(pipe, "musicbrainz_discid", @disc.id)
		add_tag(pipe, "releasestatus", @data['status'].downcase)
		add_tag(pipe, "releasetype", self.release_type)
		add_tag(pipe, "musicbrainz_releasegroupid", self.release_group_id)
		add_tag(pipe, "originaldate", self.original_date)
		add_tag(pipe, "originalyear", self.original_date)
		add_tag(pipe, "tracknumber", tracknum)
		track = self.tracks[tracknum - 1]
		artists = self.track_artists(track)
		add_tag(pipe, "title", track['title'])
		add_tag(pipe, "musicbrainz_releasetrackid", track['track-id'])
		add_tag(pipe, "musicbrainz_trackid", track['recording-id'])
		add_tag(pipe, "musicbrainz_artistid", artists[:ids])
		add_tag(pipe, "artist", artists[:names])
		add_tag(pipe, "artists", artists[:names])
		add_tag(pipe, "length", track['length'])
		if vorbis
			add_tag(pipe, "metadata_block_picture", 
				encode_image(art[:front], 3))
			add_tag(pipe, "metadata_block_picture", 
				encode_image(art[:back], 4))
			end
		pipe.close
		puts "TAGGED TRACK #{tracknum}"
		end
	def tag_tracks
		@cover_art = nil
		self.tracks.size.times do |i|
			tag_track(i + 1)
			end
		end
	def original_date
		return nil unless @data['release-group']
		return @data['release-group']['first-release-date']
		end
	def release_group_id
		return nil unless @data['release-group']
		return @data['release-group']['id']
		end
	def release_type
		return nil unless @data['release-group']
		return (@data['release-group']['primary-type'] or "").downcase
		end
	def label
		if @data['label-info'] && !(@data['label-info'].empty?)
			return @data['label-info'][0]['label']['name']
		else
			return nil
			end
		end
	def catalog_number
		if @data['label-info'] && !(@data['label-info'].empty?)
			return @data['label-info'][0]['catalog-number']
		else
			return nil
			end
		end
	def script
		if @data['text-representation']
			return @data['text-representation']['script']
		else
			return nil
			end
		end
	def tracks
		return @tracks if @tracks
		return [] unless @data['media']
		@tracks = @data['media'][0]['tracks'].sort do |a, b| 
			a['number'].to_i <=> b['number'].to_i
			end.map do |track|
				edit = {}
				edit['title'] = track['title']
				edit['length'] = track['length']
				edit['track-id'] = track['id']
				edit['recording-id'] = track['recording']['id']
				edit
				end
		return @tracks
		end
	def musicbrainz_lookup
		url = "https://musicbrainz.org/ws/2/release/#{@id}" +
			"?fmt=json&inc=labels+recordings+artists+" +
			"release-groups+artist-credits"
		begin
			json = `wget -q -O - '#{url}'`.strip
		rescue Exception => exc
			return { :error => "wget failed: #{exc.message}" }
			end
		begin
			data = JSON::parse(json)
		rescue Exception => exc
			return { :error => "invalid json: |#{json}|" }
			end
		return data
		end
	def cover_art(cache_dir, tracknum)
		return @cover_art if @cover_art
		url = "https://coverartarchive.org/release/#{@id}"
		begin
			json = `wget -q -O - '#{url}'`.strip
		rescue Exception => exc
			return @cover_art = { :error => "wget failed: #{exc.message}" }
			end
		begin
			data = JSON::parse(json)
		rescue Exception => exc
			return @cover_art = { :error => "invalid json: |#{json}|" }
			end
		unless data['images']
			@cover_art = { :error => "no images" }
			return @cover_art
			end
		@cover_art = {}
		data['images'].each do |img|
			url = nil
			if img['thumbnails'] && !(img['thumbnails'].empty?)
				url = img['thumbnails']['large'] ||
					img['thumbnails']['small']
			elsif img['image']
				url = img['image']
				end
			next unless url
			url =~ /([^\.]+)$/
			suffix = $1
			if img['front']
				file = "front_cover.#{suffix}"
				@worker.log("Track #{tracknum}: add front cover art...")
				system("wget -q -O \"#{cache_dir}/#{file}\" '#{url}'")
				@cover_art[:front] = "#{cache_dir}/#{file}"
			elsif img['back']
				file = "back_cover.#{suffix}"
				@worker.log("Track #{tracknum}: add back cover art...")
				system("wget -q -O \"#{cache_dir}/#{file}\" '#{url}'")
				@cover_art[:back] = "#{cache_dir}/#{file}"
				end
			end
		return @cover_art
		end
	end

class Disc
	# MusicBrainz disc entity
	attr_reader :releases
	attr_reader :id
	attr_reader :path
	attr_reader :data
	def initialize(worker, filepath)
		@worker = worker
		@path = filepath
		@path =~ /([^\/]+)$/
		@id = $1
		@releases = nil
		@data = self.musicbrainz_lookup
		end
	def in_musicbrainz?
		return @data[:error].nil? && releases && (releases.size == 1)
		end
	def disc_total
		return nil if @data[:error]
		return @data['releases'][0]['media'].size
		end
	def disc_num
		return nil if @data[:error]
		n = 1
		@data['releases'][0]['media'][0]['discs'].each do |disc|
			return n if disc['id'] == @id
			n += 1
			end
		return 1
		end
	def releases
		return nil if @data[:error]
		return @releases if @releases
		if @data['releases']
			@releases = data['releases'].map do |rel|
				Release.new(@worker, rel['id'], self)
				end
			end
		return @releases
		end
	def release
		if self.releases
			return @releases[0]
		else
			return nil
			end
		end
	def tag_tracks
		self.release.tag_tracks if self.release
		end
	def musicbrainz_lookup
		url = "https://musicbrainz.org/ws/2/discid/#{@id}?fmt=json"
		begin
			json = `wget -q -O - '#{url}'`.strip
		rescue Exception => exc
			return { :error => "wget failed: #{exc.message}" }
			end
		begin
			data = JSON::parse(json)
		rescue Exception => exc
			return { :error => "invalid json: |#{json}|" }
			end
		unless data.has_key?('releases')
			return { :error => "no releases" }
			end
		return data
		end
	end

class Volume
	# tracks ripped from one CD
	attr_reader :dir
	@@current = nil
	def self.current
		return @@current
		end
	def self.is_volume(dir)
		File::exists?("#{dir}/.volume")
		end
	def initialize(worker, dir, truncate = nil)
		@worker = worker
		@@current = self
		@dir = dir
		@tracks = nil
		@track_percents = []
		@disc = nil
		@submission_url = nil
		@pid = nil
		@attrs = self.load_attrs
		if truncate
			FileUtils.rm_rf(@dir) if File::exists?(@dir)
			end
		Dir::mkdir(@dir) unless File::exists?(@dir)
		if truncate
			File::open("#{@dir}/.volume", "w") do |f|
				f.write({}.to_json)
				end
			end
		end
	def submission_url
		return get_attr('submission_url')
		end
	def submission_url=(url)
		set_attr('submission_url', url)
		end
	def disc_id
		return get_attr('discid')
		end
	def disc_id=(id)
		set_attr('discid', id)
		end
	def load_attrs
		begin
			File::open("#{@dir}/.volume", "r") do |f|
				return JSON::parse(f.read)
				end
		rescue Exception => exc
			return {}
			end
		end
	def basename
		@dir =~ /([^\/]+)$/
		return $1
		end
	def get_attr(key)
		return @attrs[key]
		end
	def set_attr(key, value)
		@attrs[key] = value
		File::open("#{@dir}/.volume", "w") do |f|
			f.write(@attrs.to_json)
			end
		end
	def rip_track(device, track, encoding)
		if encoding == "flac"
			suffix = "flac"
		else
			suffix = "ogg"
			end
		outfile = "%s/track%02d.cdda.%s" % [@dir, track, suffix]
		thread = Thread.start do
			Thread::current[:state] = {'cancel' => false}
			SquishaRip.ripress(device, track,
					encoding, outfile, Thread::current[:state]) do |pct|
				yield(pct)
				end
			end
		puts "#{device} track #{track} ripped"
		return thread
		end
	def encode(encoding, bitrate, file, delete)
		success = false
		outfile = nil
		if encoding == "flac"
			outfile = file.sub(/\.wav$/, ".flac")
			frames = SquishaRip.cmp2flac(file, outfile)
			success = (frames > 0)
		else # encoding == "vorbis"
			outfile = file.sub(/\.wav$/, ".ogg")
			frames = SquishaRip.cmp2vorbis(file, outfile, 1.0)
			success = (frames > 0)
			end
		File::unlink(file) if File::exists?(file) && success && delete
		return outfile
		end
	def run_picard
		@worker.log("tagging #{@dir} with Picard...")
		files = []
		Dir::entries(@dir).sort.each do |entry|
			next unless entry =~/^track/
			files << "#{@dir}/#{entry}"
			end
		system("/usr/bin/picard #{files.join(" ")}")
		end
	def track_list(tab)
		tracks = self.tracks
		if tracks.empty?
			return ""
			end
		lines = []
		header = []
		header << tracks[0].album
		header << tracks[0].artist
		header << tracks[0].label
		lines << header.select{|s| !s.nil?}.join(", ")
		lines << "-----------------"
		n = 0
		tracks.each do |track|
			if @track_percents[n]
				pct = "[%3d%%]" % @track_percents[n]
			elsif track == (tracks.size - 1)
				pct = "[----]"
			else
				pct = "[100%]"
				end
			lines << "%6s %s %s" % [track.duration, pct, track.title]
			n += 1
			end
		tab.picard_tag_button.sensitive = true
		tab.quick_tag_button.sensitive = self.in_musicbrainz?
		return lines.join("\n")
		end
	def tracks
		@tracks =  []
		Dir::entries(@dir).sort.each do |entry|
			next unless entry =~ /^(track\d+)/
			@tracks << TrackFile.new("#{@dir}/#{entry}")
			end
		return @tracks
		end
	def set_track_percent(track, pct)
		@track_percents[track] = pct
		end
	def make_links
		trks = self.tracks
		return false if trks.empty?
		artist = trks[0].artist({:clean => true, :mbid => true})
		return false unless artist
		album_path = "#{@dir}/../../artists/#{artist}/"
		album_path << trks[0].album({:clean => true, :mbid => true})
		FileUtils::mkdir_p(album_path)
		trks.each do |track|
			track.path =~ /([^\.]+)$/
			suffix = $1
			title = ("%02d" % track.tracknum) +
				":" + track.title(:clean) + "." + suffix
			target = Pathname.new(track.path)
			FileUtils::ln_s(target.relative_path_from(album_path),
				"#{album_path}/#{title}", :force => true)
			end
		return true
		end
	def musicbrainz_disc
		return @disc if @disc
		return (@disc = Disc.new(@worker, @dir))
		end
	def in_musicbrainz?
		self.musicbrainz_disc.in_musicbrainz?
		end
	def tag_direct
		self.musicbrainz_disc.tag_tracks
		end
	end

def asciify(src)
	out = src.gsub(/\s+/, " ").gsub("/", "-")
	out = I18n::transliterate(out, :locale => :en)
	return out
	end

class Collection
	# collection of ripped volumes
	attr_reader :dir
	attr_reader :voldir
	def initialize(worker, dir)
		@worker = worker
		@dir = File::realpath(dir)
		provision
		@voldir = "#{@dir}/volumes"
		end
	def provision
		return unless self.writable
		mk_subdir # this dir
		mk_subdir("volumes")
		mk_subdir("artists")
		end
	def mk_subdir(subdir = "")
		path = "#{@dir}/#{subdir}"
		Dir::mkdir(path) unless File::exists?(path)
		end
	def has_volume?(disc_id)
		path = "#{@voldir}/#{disc_id}"
		return File::exists?(path) && File::exists?("#{path}/.volume")
		end
	def volumes
		Dir::entries(@voldir).select{|entry| entry !~ /^\./}
		end
	def new_volume(base_name, submission_url)
		return nil unless self.writable
		self.provision
		volume = Volume.new(@worker, "#{@voldir}/#{base_name}", :truncate)
		volume.submission_url = submission_url
		volume.disc_id = base_name
		return volume
		end
	def writable
		begin
			Dir::chdir(@dir)
		rescue Exception => exc
			return false
			end
		tmp = "#{@dir}/#{ENV['USER']}-#{Time.now.to_i}-#{rand(2 ** 32)}"
		begin
			File::open(tmp, "w") { |f| f.write("foo") }	
			File::unlink(tmp)
		rescue Exception => exc
			return false
			end
		return true
		end
	end

class WorkBox < Gtk::Box
	attr_reader :picard_tag_button
	attr_reader :quick_tag_button
	def initialize(app, cd_drive)
		super(:vertical)
		@app = app
		@drive = cd_drive
		@volume = nil
		@track_thread = nil
		@rip_handler = nil
		self.spacing = 3
		grid = Gtk::Grid.new
		grid.set_property("row-homogeneous", true)
		grid.set_property("column-homogeneous", false)
		grid.set_property("row-spacing", 0)
		self.add(grid)
		row = 0
		row += chooser_block(grid, row)
		@device_status = Gtk::Label.new("DEVICE")
		@device_status.xalign = 0
		grid.attach(@device_status, 1, row, 1, 1)
		row += 1
		encoding_buttons(grid, row)
		self.add(log_block)
		self.add(button_block)
		self.add(payload_block)
		self.collection = @app.prefs['collection']
		cd_watch
		end
	def qualify_tab_title(addendum)
		# reflect rip job progress
		child = @app.tabs.get_child_by_name(@drive[:device])
		if child
			title = "#{@drive[:name]} #{addendum}"
			@app.tabs.child_set_property(child, "title", title)
			end
		end
	def encoding
		return "vorbis" if @vorbis_encode.active?
		return "flac"
		end
	def encoding_buttons(grid, row)
		label = Gtk::Label.new("Compression: ")
		label.xalign = 1
		grid.attach(label, 0, row, 1, 1)
		box = Gtk::Box.new(:horizontal)
		box.spacing = 6
		@flac_encode = Gtk::RadioButton.new(:label => "FLAC")
		@flac_encode.set_active(@app.prefs['encoding'] == "flac")
		@flac_encode.signal_connect("clicked") do
			@app.set_pref('encoding', self.encoding)
			end
		box.add(@flac_encode)
		@vorbis_encode = Gtk::RadioButton.new(:member => @flac_encode,
			:label => "Vorbis")
		@vorbis_encode.set_active(@app.prefs['encoding'] == "vorbis")
		box.add(@vorbis_encode)
		box.tooltip_text = "select audio compression"
		grid.attach(box, 1, row, 1, 1)
		return 1
		end
	def log_block
		scroller = Gtk::ScrolledWindow.new
		scroller.set_policy(:automatic, :automatic)
		scroller.height_request = 100
		scroller.shadow_type = Gtk::ShadowType::IN
		@logger = Gtk::TextView.new
		@logger.signal_connect("size-allocate") do
			scroller.vadjustment.value = scroller.vadjustment.upper -
				scroller.vadjustment.page_size
			end
		@logger.tooltip_text = "activity log"
		@logger.buffer.text = ""
		scroller.add(@logger)
		return scroller
		end
	def payload_block
		scroller = Gtk::ScrolledWindow.new
		scroller.set_policy(:automatic, :automatic)
		scroller.vexpand = true
		scroller.shadow_type = Gtk::ShadowType::IN
		@payload = Gtk::TextView.new
		@payload.signal_connect("size-allocate") do
			scroller.vadjustment.value = scroller.vadjustment.upper -
				scroller.vadjustment.page_size
			end
		@payload.tooltip_text = "disc volume content"
		scroller.add(@payload)
		return scroller
		end
	def chooser_block(grid, row)
		label = Gtk::Label.new("Collection/Volume: ")
		label.xalign = 1
		grid.attach(label, 0, row, 1, 1)
		button = Gtk::Button.new(label: "Select")
		button.signal_connect("clicked") do |widget|
			dialog = Gtk::FileChooserDialog.new(
				:title => "Choose Collection or Volume",
				:parent => @main,
				:action => Gtk::FileChooserAction::SELECT_FOLDER,
				:buttons => [
					[Gtk::Stock::CANCEL, Gtk::ResponseType::CANCEL],
					[Gtk::Stock::OPEN, Gtk::ResponseType::ACCEPT]])
			dialog.current_folder = @collection.dir if @collection
			dialog.create_folders = true
			if dialog.run == Gtk::ResponseType::ACCEPT
				dir = dialog.filename
				if Volume::is_volume(dir)
					@chooser_status.text = "Volume: #{dir}"
					vol = Volume.new(self, dir)
					@submit_cd_button.sensitive =
						!(vol.in_musicbrainz?) && vol.submission_url
					self.volume_report(vol.track_list(self))
				else
					self.collection = dir
					self.volume_report("")
					end
				end
			dialog.destroy
			end
		button.tooltip_text =
			"select collection for ripping to or volume for review"
		grid.attach(button, 1, row, 1, 1)
		@chooser_status = Gtk::Label.new("status")
		@chooser_status.xalign = 0
		grid.attach(@chooser_status, 1, row + 1, 1, 1)
		return 2
		end
	def launch_rip_job
		volname = already_ripped
		if volname
			rip_anyway = ripped_dialog(volname)
			return unless rip_anyway
			end
		Thread.start do
			@app.register_rip_thread(Thread::current)
			@picard_tag_button.sensitive = false
			@quick_tag_button.sensitive = false
			@rip_button.set_label("Cancel Rip");
			@rip_button.signal_handler_disconnect(@rip_handler)
			@rip_handler = @rip_button.signal_connect("clicked") do
				stop_rip
				end
			volume = rip_cd(DefaultBitrate)
			if volume.in_musicbrainz?
				log("This disc is known to MusicBrainz.")
				@quick_tag_button.sensitive = true
				@submit_cd_button.sensitive = false
				end
			@picard_tag_button.sensitive = true
			@app.unregister_rip_thread(Thread::current)
			@rip_button.set_label("Rip/Compress");
			@rip_button.signal_handler_disconnect(@rip_handler)
			@rip_handler = @rip_button.signal_connect("clicked") do
				launch_rip_job
				end
			end
		end
	def button_block
		box = Gtk::Box.new(:horizontal)
		box.spacing = 6
		@rip_button = Gtk::Button.new(label: "Rip/Compress")
		@rip_button.sensitive = false
		@rip_button.width_request = ButtonWidth
		@rip_button.tooltip_text = "begin CD ripping and audio compression"
		box.add(@rip_button)
		@picard_tag_button = Gtk::Button.new(label: "Picard Tag")
		@picard_tag_button.sensitive = false
		@picard_tag_button.width_request = ButtonWidth
		@picard_tag_button.tooltip_text = "tag with MusicBrainz Picard"
		box.add(@picard_tag_button)
		@quick_tag_button = Gtk::Button.new(label: "Quick Tag")
		@quick_tag_button.sensitive = false
		@quick_tag_button.width_request = ButtonWidth
		@quick_tag_button.tooltip_text = "tag with direct MusicBrainz lookup"
		box.add(@quick_tag_button)
		@submit_cd_button = Gtk::Button.new(label: "Submit CD")
		@submit_cd_button.sensitive = false
		@submit_cd_button.width_request = ButtonWidth
		@submit_cd_button.tooltip_text = "submit CD info to MusicBrainz"
		box.add(@submit_cd_button)
		@rip_handler = @rip_button.signal_connect("clicked") do
			launch_rip_job
			end
		@picard_tag_button.signal_connect("clicked") do
			unless Volume::current
				log("No volume selected.")
				next
				end
			Thread.start do
				Volume::current.run_picard
				if Volume::current.make_links
					Volume::current.set_attr("tagged", Time.now.to_i)
					Volume::current.set_attr("linked", Time.now.to_i)
					log("Linked to volume from artists catalog.")
					self.volume_report(Volume::current.track_list(self))
				else
					log("Tagging not complete, no links made.")
					@submit_cd_button.sensitive = true
					end
				end
			end
		@quick_tag_button.signal_connect("clicked") do
			unless Volume::current
				log("No volume selected.")
				next
				end
			Thread.start do
				if Volume::current.in_musicbrainz?
					Volume::current.tag_direct
				else
					log("This disc ID is not known to MusicBrainz.")
					Thread::exit
					end
				if Volume::current.make_links
					Volume::current.set_attr("tagged", Time.now.to_i)
					Volume::current.set_attr("linked", Time.now.to_i)
					log("Linked to volume from artists catalog.")
					self.volume_report(Volume::current.track_list(self))
				else
					log("Tagging not complete, no links made.")
					end
				end
			end
		@submit_cd_button.signal_connect("clicked") do
			if Volume::current && !(Volume::current.in_musicbrainz?) &&
					Volume::current.submission_url
				Thread.start do
					log("Opening CD submission in separate window.")
					system("xdg-open '#{Volume::current.submission_url}'")
					end
				end
			end
		return box
		end
	def collection=(dir)
		@collection = Collection.new(self, dir)
		if @collection.writable
			@chooser_status.text =
				"Collection: #{@collection.dir} [WRITABLE]"
			@app.set_pref('collection', dir)
		else
			@chooser_status.text =
				"Collection: #{@collection.dir} [NOT WRITABLE]"
			end
		@rip_button.sensitive = @collection.writable
		end
	def cd_watch
		Thread.start do
			prev_disc_id = nil
			while true
				sleep(CDWatchInterval)
				unless @drive
					self.status = ""
					self.volume_report("")
					prev_disc_id = nil
					next
					end
				begin
					disc = DiscId::read(@drive[:device])
					next if disc.id == prev_disc_id
					prev_disc_id = disc.id
					self.status = "#{disc.tracks.size} tracks," +
						" Disc ID '#{disc.id}'"
					if @collection && @collection.has_volume?(disc.id)
						self.volume_report("checking volume...")
						Thread.start do
							vol = Volume.new(self, @collection.voldir +
								"/#{disc.id}")
							unless vol.submission_url
								vol.submission_url =
									disc.submission_url
								end
							self.volume_report(vol.track_list(self))
							end
						end
					@rip_button.sensitive = @collection.writable
				rescue Exception => exc
					@rip_button.sensitive = false
					puts exc.inspect if Debugging
					self.status = "No audio CD in this drive..."
					self.volume_report("")
					prev_disc_id = nil
					next
					end
				end
			end
		end
	def status=(msg)
		@device_status.text = msg
		end
	def volume_report(content)
		return unless @payload
		@payload.buffer.text = content
		end
	def already_ripped
		disc_id = DiscId::read(@drive[:device]).id
		@collection.volumes.each do |vol|
			fullpath = "#{@collection.voldir}/#{vol}"
			puts [disc_id, vol, fullpath].inspect
			next unless File::exists?(fullpath)
			next unless File::directory?(fullpath)
			next unless Volume::is_volume(fullpath)
			return vol if vol == disc_id
			volfile = "#{fullpath}/.volume"
			begin
				meta = File::open(volfile) {|f| JSON::parse(f.read)}
				return vol if meta["discid"] == disc_id
			rescue Exception => exc
				return nil
				end
			end
		return nil
		end
	def ripped_dialog(volname)
		fullpath = "#{@collection.voldir}/#{volname}"
		md = Gtk::MessageDialog.new(
			:title => "Already Ripped",
			:type => :info,
			:parent => @main,
			:message =>
				"This CD seems to have already been ripped to\n\n" +
				"\"#{fullpath}\"\n\nRe-rip anyway?",
			)
		md.add_buttons(
			[Gtk::Stock::CANCEL, Gtk::ResponseType::CANCEL],
#			[Gtk::Stock::OK, Gtk::ResponseType::OK]
#	why is there already an OK button?
			)
		resp = md.run
		md.destroy
		resp == Gtk::ResponseType::OK
		end
	def stop_rip
		@track_thread[:state]['cancel'] = true if @track_thread
		end
	def rip_cd(bitrate)
		disc = DiscId::read(@drive[:device])
		volume = @collection.new_volume(disc.id, disc.submission_url)
		tracks = disc.tracks.size
		log("Ripping #{tracks} CD tracks to ./#{volume.basename}...")
		volume_report("")
		qualify_tab_title("0/#{tracks}")
		cancelled = false
		tracks.times do |track|
			track1 = track + 1
			log("Ripping/compressing track #{track1}")
			@track_thread = volume.rip_track(@drive[:device],
						track1, encoding) do |pct|
				volume.set_track_percent(track, pct)
				volume_report(volume.track_list(self))
				end
			@track_thread.join
			if @track_thread[:state]['cancel']
				log("Rip cancelled")
				@track_thread = nil
				cancelled = true
				break
				end
			@track_thread = nil
			qualify_tab_title("#{track1}/#{tracks}")
			break if @app.shutdown
			end
		unless @app.shutdown || cancelled
			volume.set_attr("ripped", Time.now.to_i)
			volume.set_attr("compressed", Time.now.to_i)
			name = @drive[:name]
			@app.send_email("Picarip job #{name}",
				"Ripping/compression job on device #{name} is complete.")
			volume_report(volume.track_list(self))
			log("Ripping/compression complete.")
			end
		return volume
		end
	def log(msg)
		buffer = @logger.buffer
		stamp = Time.now.strftime("%m/%d %H:%M:%S")
		buffer.insert(buffer.end_iter, "#{stamp} #{msg}\n")
		end
	end

class ThisApp < Gtk::Application
	attr_reader :collection
	attr_reader :file_list
	attr_reader :prefs
	attr_reader :tabs
	attr_reader :shutdown
	def initialize(id)
		super("net.wtju.picarip" + id.to_s, :flags_none)
		@prefs_file = "#{ENV['HOME']}/.picariprc"
		@prefs = self.load_prefs
		@shutdown = false
		@rip_threads = []
		self.signal_connect("activate") do |app|
			@main = Gtk::ApplicationWindow.new(app)
			@main.signal_connect("destroy") do |wid|
				@shutdown = true
				SquishaRip.shutdown
				@rip_threads.each do |thr|
					thr.join
					end
				end
			@main.set_title("Picarip CD Ripper/Tagger")
			@main.set_default_size(600, 500)
			@main.set_border_width(10)
			bigbox = Gtk::Box.new(:vertical)
			bigbox.spacing = 3
			grid = Gtk::Grid.new
			grid.set_property("row-homogeneous", true)
			grid.set_property("column-homogeneous", false)
			grid.set_property("row-spacing", 0)
			row = 0
			row += email_block(grid, row)
			@workers = []
			@tabs = Gtk::Stack.new
			switcher = Gtk::StackSwitcher.new
			switcher.height_request = 10
			cdrom_drives.each do |drive|
				worker = WorkBox.new(app, drive)
				@workers << worker
				@tabs.add_titled(worker, drive[:device], drive[:name])
				end
			switcher.set_stack(@tabs)
			bigbox.pack_start(grid,
				:expand => false, :fill => true, :padding => 0)
			bigbox.pack_start(Gtk::Separator.new(:horizontal),
				:expand => false, :fill => true, :padding => 0)
			bigbox.pack_start(switcher,
				:expand => false, :fill => true, :padding => 0)
			bigbox.pack_start(@tabs,
				:expand => false, :fill => true, :padding => 0)
			@main.add(bigbox)
			@main.show_all
			end
		self.signal_connect("shutdown") do |app|
			save_prefs
			puts "bye bye"
			end
		end
	def register_rip_thread(thread)
		@rip_threads << thread
		end
	def unregister_rip_thread(thread)
		@rip_threads.delete(thread)
		end
	def cdrom_drives
		all_drives = []
		can_play = []
		IO::readlines(CDROM_INFO).each do |line|
			next unless line =~ /^([^:]+):\s*(.+)/
			key = $1.strip.downcase
			data = $2.strip.split(/\s+/)
			if key == "drive name"
				all_drives = data
			elsif key == "can play audio"
				can_play = data
				end
			end
		audio_drives = []
		all_drives.size.times do |i|
			audio_drives << all_drives[i] if can_play[i].to_i == 1
			end
		return audio_drives.sort.map do |d|
			{ :device => "/dev/#{d}", :name => device_name(d)}
			end
		end
	def device_name(dev)
		dev =~ /(\d+)$/
		num = $1.to_i
		return "Int CD" if num.to_i == 0
		return "Ext CD-#{num}"
		end
	def email_block(grid, row)
		label = Gtk::Label.new("Email Alerts To: ")
		label.xalign = 1
		grid.attach(label, 0, row, 1, 1)
		box = Gtk::Box.new(:horizontal)
		@email_text = Gtk::Entry.new
		@email_text.text = @prefs['email'] || ""
		@email_text.tooltip_text =
			"send email when ripping/compression completes"
		@email_text.signal_connect("changed") do
			self.set_pref('email', @email_text.text)
			end
		box.add(@email_text)
		@email_enable = Gtk::CheckButton.new("Alerts Enabled")
		@email_enable.tooltip_text = "enable email notification"
		@email_enable.set_active(false)
		box.add(@email_enable)
		grid.attach(box, 1, row, 1, 1)
		return 1
		end
	def send_email(subject, msg)
		return unless @email_enable.active?
		@email_text.text.strip.split(/[ ,]+/).each do |addr|
			next unless addr =~ /.+@.+/
			cmd = "mail -s \"#{subject.gsub('"', "")}\""
			cmd << " '#{addr.gsub("'", "")}'"
			pipe = IO::popen(cmd, "w")
			pipe.puts msg
			pipe.close
			end
		end
	def load_prefs
		data = {}
		begin
			json = File.open(@prefs_file, "r") { |f| f.read }
			data = JSON::parse(json)
		rescue Exception => exc
			end
		data['encoding'] ||= "vorbis"
		data['collection'] ||= ENV['HOME']
		return data
		end
	def save_prefs
		File.open(@prefs_file, "w") do |f|
			f.write(@prefs.to_json)
			end
		end
	def set_pref(key, value)
		@prefs[key] = value
		self.save_prefs
		end
	end

I18n.config.available_locales = :en

Random::srand
ThisApp.new(Process::pid).run
