#! /usr/bin/ruby

require "rubygems"
require "pp"
require "discid"
require "taglib"
require "fileutils"
require "i18n"

DeleteWav = true
Rename = true
WatcherFile = "watcher"
PIDFile = "pid"
DefaultBitrate = 320
Terminals = [
	"/usr/bin/gnome-terminal",
	"/usr/bin/xfce4-terminal",
	"/usr/bin/mate-terminal",
	]
Rippers = [
	"/usr/bin/cd-paranoia",
	"/usr/bin/cdparanoia",
	]
WatcherScript = <<END
#! /bin/bash

echo $$ > #{PIDFile}
exec watch -t 'echo ripping to PATH; ls -l 2>/dev/null track*'
END
Usage = "picarip.rb [--archive <archive dir>] [--device <CD device>]\
 [--no-rip] [--vorbis] [--bitrate]"

def ripper
	Rippers.each do |ripper|
		return ripper if File::exists?(ripper)
		end
	return Rippers.last
	end

def terminal
	Terminals.each do |term|
		return term if File::exists?(term)
		end
	return "/usr/bin/xterm"
	end

def open_monitor
	File.open(WatcherFile, "w") do |file|
		file.puts WatcherScript.sub("PATH", File::realpath("."))
		end
	File::chmod(0755, WatcherFile)
	#system("#{terminal} -e ./#{WatcherFile}")
	system("#{terminal} -- ./#{WatcherFile}")
	end

def close_monitor
	if File::exists?(PIDFile)
		pid = File.open(PIDFile, "r") { |f| f.read.strip.to_i }
		Process::kill("TERM", pid)
		File::unlink(PIDFile)
		end
	File::unlink(WatcherFile)
	end

def munged_album(tags)
	# massage album title to make a file-system-valid dir name
	title = tags['ALBUM']
	return nil unless title
	title = title[0].gsub(/\s+/, "_").gsub("/", "-")
	title = I18n::transliterate(title, :locale => :en)
	tags['MUSICBRAINZ_ALBUMID'][0] =~ /^([^-]+)/
	title << "::#{$1}"
	disc = "%02d%02d" %
		[tags['DISCNUMBER'][0].to_i, tags['TOTALDISCS'][0].to_i]
	title << "::#{disc}"
	return title
	end

def encode(encoding, bitrate, file, delete)
	success = false
	outfile = nil
	if encoding == :flac
		outfile = file.sub(/\.wav$/, ".flac")
		success = system("/usr/bin/flac -f -o #{outfile} #{file}")
	elsif encoding == :vorbis
		outfile = file.sub(/\.wav$/, ".ogg")
		success = system("/usr/bin/oggenc -b #{bitrate} -o #{outfile} #{file}")
		end
	File::unlink(file) if success && delete
	return outfile
	end

def get_tags(encoding, work_dir)
	tagset = {}
	if encoding == :flac
		TagLib::FLAC::File.open("#{work_dir}/track01.cdda.flac") do |file|
			tagset = file.xiph_comment.field_list_map
			end
	elsif encoding == :vorbis
		TagLib::Ogg::Vorbis::File.open("#{work_dir}/track04.cdda.ogg") do |file|
			tagset = file.tag.field_list_map
			end
		end
	return tagset
	end

I18n.config.available_locales = :en

archive = "."
device = DiscId::default_device
valid_args = true
bitrate = DefaultBitrate
rip = true
encoding = :flac
while !ARGV.empty?
	term = ARGV.shift
	if term == "--archive"
		archive = ARGV.shift
	elsif term == "--device"
		device = ARGV.shift
	elsif term == "--no-rip"
		rip = false
	elsif term == "--bitrate"
		bitrate = ARGV.shift.to_i
		if bitrate == 0
			puts "invalid bitrate"
			valid_args = false
			end
	elsif term == "--vorbis"
		encoding = :vorbis
	elsif term == "--help"
		valid_args = false
	else
		puts "invalid arg: #{term}"
		valid_args = false
		end
	end

unless valid_args
	puts Usage
	exit
	end

while true
	begin
		work_dir = DiscId::read(device).id
		break
	rescue DiscId::DiscError
		puts "No audio CD in drive #{device}? (ctrl-C to quit)"
		sleep 5
		end
	end

puts "Ripping from #{device}" if rip
puts "Archiving to #{archive}"

# prepare and enter work directory
Dir::chdir(archive)
FileUtils.rm_rf(work_dir) if rip
Dir::mkdir(work_dir) unless File::exists?(work_dir)
Dir::chdir(work_dir)

# monitor progress in a separate shell window
open_monitor

# rip CD tracks to WAV files
if rip
	system("#{ripper} --output-wav -B")
	File::unlink("track00.cdda.wav") if File::exists?("track00.cdda.wav")
	end

# encode to compressed audio
files = []
if rip
	Dir::glob("track*.wav").sort.each do |path|
		files << encode(encoding, bitrate, path, DeleteWav)
		end
else
	Dir::glob("track*").sort.each do |path|
		files << path
		end
	end

close_monitor

# tag compressed files
puts "launching Picard, please wait..."
system("/usr/bin/picard #{files.join(" ")}") unless encoding == :none

# derive album title and rename work directory
Dir::chdir("..")
tags = get_tags(encoding, work_dir)
new_dir = munged_album(tags)
if new_dir
	if Rename
		puts "RENAME #{work_dir} -> #{new_dir}"
		FileUtils.rm_rf(new_dir) if File::exists?(new_dir)
		File::rename(work_dir, new_dir)
		end
else
	puts "MISSING METADATA"
	end
