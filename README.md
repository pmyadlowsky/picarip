# picarip
Audio CD ripper that consults MusicBrainz' Picard tagger

This is a ruby script that cobbles together several audio utilities to:
* rip tracks from an audio CD, render as WAV files
* encode WAVs to compressed form; FLAC, Vorbis, MP3
* invoke Picard to tag the compressed audio files

## Dependencies

### Utilities

* cdparanoia
* flac
* vorbis-tools (oggenc)
* picard

### Libraries and Packages

* ruby-dev
* build-essential
* libdiscid0
* libtagc0-dev

### Ruby Gems

* rake
* discid
* taglib-ruby
* i18n
