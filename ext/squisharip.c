#include <ruby.h>
#include <sndfile.h>
#include <stdio.h>
#include <stdlib.h>
#include <cdio/paranoia/paranoia.h>
#include <cdio/cd_types.h>
#include <pthread.h>

#define CDAUDIO_CHANNELS 2
#define CDAUDIO_SRATE 44100
#define FRAMEBUF 8192
#define RINGBUF_SIZE (CDIO_CD_FRAMESIZE_RAW * 1024)

static int pinch_off = 1;
static int shutdown = 0;

typedef struct ripjob {
	track_t track;
	lsn_t first_lsn;
	lsn_t last_lsn;
	SNDFILE *sndfile;
	int16_t ringbuf[RINGBUF_SIZE];
	int rbhead;
	int rbtail;
	int compressing;
	pthread_cond_t compress_cond;
	pthread_mutex_t compress_mutex;
	cdrom_drive_t *drive;
	} RIPJOB;

static int compress(const char *infile, const char *outfile,
		int format, double quality) {
	SF_INFO in_info;
	SNDFILE *in = sf_open(infile, SFM_READ, &in_info);
	if (in == NULL) {
		rb_raise(rb_eRuntimeError, "failed to open %s for reading: %s\n",
			infile, sf_strerror(NULL));
		return 0;
		}
	SF_INFO out_info;
	out_info.samplerate = in_info.samplerate;
	out_info.channels = in_info.channels;
	out_info.format = format;
	SNDFILE *out = sf_open(outfile, SFM_WRITE, &out_info);
	if (out == NULL) {
		sf_close(in);
		rb_raise(rb_eRuntimeError, "failed to open %s for writing: %s\n",
			outfile, sf_strerror(NULL));
		return 0;
		}
	if (quality >= 0.0) {
		sf_command(out, SFC_SET_VBR_ENCODING_QUALITY,
			(void *)&quality, sizeof(double));
		}
	int frames = 0;
	int n;
	double *buf = (double *)malloc(FRAMEBUF *
		in_info.channels * sizeof(double));
	while ((n = sf_readf_double(in, buf, FRAMEBUF)) > 0) {
		sf_writef_double(out, buf, n);
		frames += n;
		}
	free(buf);
	sf_close(in);
	sf_close(out);
	return frames;
	}

static VALUE cmp2flac(VALUE self, VALUE infile, VALUE outfile) {
	int frames = compress(StringValuePtr(infile), StringValuePtr(outfile),
			SF_FORMAT_FLAC | SF_FORMAT_PCM_24, -1.0);
	return INT2NUM(frames);
	}

static VALUE cmp2vorbis(VALUE self, VALUE infile, VALUE outfile,
		VALUE quality) {
	int frames = compress(StringValuePtr(infile), StringValuePtr(outfile),
			SF_FORMAT_OGG | SF_FORMAT_VORBIS, NUM2DBL(quality));
	return INT2NUM(frames);
	}

static VALUE symbol(const char *name) {
	return ID2SYM(rb_intern(name));
	}

static VALUE file_info(VALUE self, VALUE file) {
	const char *path = StringValuePtr(file);
	VALUE hash = rb_hash_new();
	SF_INFO info;
	SNDFILE *in = sf_open(path, SFM_READ, &info);
	if (in == NULL) {
		rb_raise(rb_eRuntimeError, "failed to open %s for info: %s\n",
			path, sf_strerror(NULL));
		return hash;
		}
	rb_hash_aset(hash, symbol("samplerate"), INT2NUM(info.samplerate));
	rb_hash_aset(hash, symbol("channels"), INT2NUM(info.channels));
	rb_hash_aset(hash, symbol("frames"), INT2NUM(info.frames));
	const char *format;
	if (info.format & SF_FORMAT_WAV) format = "WAV";
	else if (info.format & SF_FORMAT_FLAC) format = "FLAC";
	else if (info.format & SF_FORMAT_OGG) format = "OGG";
	else format = "OTHER";
	rb_hash_aset(hash, symbol("format"), symbol(format));
	sf_close(in);
	return hash;
	}

static void *compressor(void *data) {
	// Watch for incoming audio in ring buffer, dump to audio file.
	RIPJOB *job = (RIPJOB *)data;
	int samples, block;
	int head_cache;
	printf("start compressor\n");
	pthread_mutex_lock(&(job->compress_mutex));
	while (job->compressing) {
		samples = 0;
		while (job->rbtail != job->rbhead) {
			block = 0;
			head_cache = job->rbhead; // take snapshot of head position
			if (job->rbtail < head_cache) block = head_cache - job->rbtail;
			else block = RINGBUF_SIZE - job->rbtail;
			sf_write_short(job->sndfile, job->ringbuf + job->rbtail, block);
			job->rbtail += block;
			if (job->rbtail >= RINGBUF_SIZE) job->rbtail -= RINGBUF_SIZE;
			samples += block;
			}
		printf("write %d, wait...\n", samples);
		pthread_cond_wait(&(job->compress_cond),
			&(job->compress_mutex));
		}
	pthread_mutex_unlock(&(job->compress_mutex));
	printf("quit compressor\n");
	return NULL;
	}

static int nudge_compressor(RIPJOB *job) {
	// tell waiting compressor that there's new audio data
	if (pthread_mutex_trylock(&(job->compress_mutex)) == 0) {
		pthread_cond_signal(&(job->compress_cond));
		pthread_mutex_unlock(&(job->compress_mutex));
		return 1;
		}
	return 0;
	}

static int truncate_silence(int16_t *buf, int frames) {
	// remove silence from end of ripped track's final sector
	int16_t *cursor = buf + frames * CDAUDIO_CHANNELS - 1;
	int lopped = 0;
	while ((cursor > buf) && (*cursor == 0) && (*(cursor - 1) == 0)) {
		frames -= 1;
		lopped += 1;
		cursor -= CDAUDIO_CHANNELS;
		}
	printf("lopped off %d frames\n", lopped);
	return frames;
	}

//static void *ripress_thread(void *data) {
static void *ripress_thread(RIPJOB *job, VALUE state, VALUE block) {
//	RIPJOB *job = (RIPJOB *)data;
	// rip audio from CD track, stuff it into ring buffer
	pthread_t compress_thread;
	cdda_verbose_set(job->drive, CDDA_MESSAGE_PRINTIT, CDDA_MESSAGE_PRINTIT);
	cdrom_paranoia_t *p = paranoia_init(job->drive);
	lsn_t cursor;
	printf("reading track %d from LSN %ld to LSN %ld\n", job->track,
		(long int)job->first_lsn, (long int)job->last_lsn);
	paranoia_modeset(p, PARANOIA_MODE_FULL ^ PARANOIA_MODE_NEVERSKIP);
	paranoia_seek(p, job->first_lsn, SEEK_SET);
	int frames, samples;
	char *psz_err;
	char *psz_msg;
	int16_t *p_readbuf;
	VALUE cancel_key = rb_str_new2("cancel");
	RB_GC_GUARD(cancel_key);
	VALUE cancel;
	int pct, prev_pct, i;
	prev_pct = -1;
	int span = job->last_lsn - job->first_lsn;
	pthread_create(&compress_thread, NULL, compressor, (void *)job);
	for (cursor = job->first_lsn; cursor <= job->last_lsn; cursor++) {
		if (shutdown) break;
		cancel = rb_hash_aref(state, cancel_key);
		if (cancel == Qtrue) break;
		p_readbuf = paranoia_read(p, NULL);
		psz_err = cdda_errors(job->drive);
		psz_msg = cdda_messages(job->drive);
		if (psz_msg || psz_err)
			printf("%s%s\n", psz_msg ? psz_msg: "", psz_err ? psz_err: "");
		free(psz_err);
		free(psz_msg);
		if (p_readbuf == NULL) {
			rb_raise(rb_eRuntimeError, "paranoia read error");
			break;
			}
		samples = CDIO_CD_FRAMESIZE_RAW / sizeof(int16_t);
		frames = samples / CDAUDIO_CHANNELS;
		if (pinch_off && (cursor == job->last_lsn)) {
			frames = truncate_silence(p_readbuf, frames);
			}
		for (i = 0; i < frames; i++) {
			job->ringbuf[job->rbhead] = *p_readbuf++;
			job->ringbuf[job->rbhead + 1] = *p_readbuf++;
			job->rbhead += CDAUDIO_CHANNELS;
			if (job->rbhead >= RINGBUF_SIZE) job->rbhead -= RINGBUF_SIZE;
			if (job->rbhead == job->rbtail) printf("OVERRUN!\n");
			}
		nudge_compressor(job);
		pct = ((cursor - job->first_lsn) * 100) / span;
		if (pct != prev_pct) {
			prev_pct = pct;
			rb_funcall(block, rb_intern("call"), 1, INT2NUM(pct));
			}
		}
	job->compressing = 0;
printf("shut down compressor, waiting...\n");
	while (!nudge_compressor(job));
	pthread_join(compress_thread, NULL);
printf("compressor down\n");
	paranoia_free(p);
	return NULL;
	}

static VALUE ripress(VALUE self, VALUE rb_drive, VALUE rb_track,
		VALUE rb_format, VALUE filepath, VALUE thread_state) {
	// set up and launch rip/compression job
	pthread_t ripthread;
	RIPJOB *job;
	const char *drive = StringValuePtr(rb_drive);
	const char *format = StringValuePtr(rb_format);
	track_t track = NUM2INT(rb_track);
	printf("DRIVE %s, TRACK %d\n", drive, track);
	cdrom_drive_t *drv = cdda_identify(drive, 1, NULL);
	if (drv == NULL) {
		rb_raise(rb_eRuntimeError, "unable to identify CD drive %s",
			drive);
		return Qnil;
		}
	if (cdda_open(drv) != 0) {
		rb_raise(rb_eRuntimeError, "can't open drive %s\n", drive);
		return Qnil;
		}
	lsn_t first_lsn = cdda_track_firstsector(drv, track);
	lsn_t last_lsn = cdda_track_lastsector(drv, track);
	if ((first_lsn < 0) || (last_lsn < 0)) {
		rb_raise(rb_eRuntimeError, "invalid track: %d\n", track);
		return Qnil;
		}
	job = (RIPJOB *)malloc(sizeof(RIPJOB));
	job->track = track;
	job->drive = drv;
	job->first_lsn = first_lsn;
	job->last_lsn = last_lsn;
	job->rbhead = job->rbtail = 0;
	job->compressing = 1;
	pthread_cond_init(&(job->compress_cond), NULL);
	pthread_mutex_init(&(job->compress_mutex), NULL);
	SF_INFO info;
	info.samplerate = CDAUDIO_SRATE;
	info.channels = CDAUDIO_CHANNELS;
	if (strcmp(format, "flac") == 0)
		info.format = SF_FORMAT_FLAC | SF_FORMAT_PCM_24;
	else
		info.format = SF_FORMAT_OGG | SF_FORMAT_VORBIS;
	job->sndfile = sf_open(StringValuePtr(filepath), SFM_WRITE, &info);
	double quality = 1.0;
	sf_command(job->sndfile, SFC_SET_VBR_ENCODING_QUALITY,
		(void *)&quality, sizeof(double));
	VALUE block = rb_block_proc();
	ripress_thread(job, thread_state, block);
	cdda_close(drv);
	sf_close(job->sndfile);
	free(job);
	return Qnil;
	}

static VALUE shutdown_rippers(VALUE self) {
	shutdown = 1;
	return Qnil;
	}

void Init_squisharip() {
	VALUE module = rb_define_module("SquishaRip");
	rb_define_module_function(module, "cmp2flac", cmp2flac, 2);
	rb_define_module_function(module, "cmp2vorbis", cmp2vorbis, 3);
	rb_define_module_function(module, "file_info", file_info, 1);
	rb_define_module_function(module, "ripress", ripress, 5);
	rb_define_module_function(module, "shutdown", shutdown_rippers, 0);
	return;
	}
